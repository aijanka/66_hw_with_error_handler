import React, {Component} from 'react';
import {Grid} from "react-bootstrap";
import axios from 'axios';
import PostShort from "../../components/PostShort/PostShort";
import withLoader from "../../hoc/withLoader/withLoader";
import ErrorBoundary from "../../components/UI/ErrorBoundary/ErrorBoundary";

class PostsList extends Component {
    state = {
        posts: []
    };

    componentDidMount() {
        this.getPosts();
    }

    getPosts() {
        axios.get('/posts.json').then(response => {
            if (response) {
                this.setState({posts: response.data});
            }
        })
    }

    render() {
        const keys = Object.keys(this.state.posts);
        const posts = keys.map(id => {
            const post = this.state.posts[id];
            return (
                <ErrorBoundary key={id}>
                    <PostShort
                        date={post.date}
                        id={id}
                        title={post.title}
                        body={post.body}
                    />
                </ErrorBoundary>
            );
        });
        if (this.state.posts) {
            return (
                <Grid>{posts}</Grid>
            )
        } else {
            return <h1>No posts yet</h1>;
        }
    }
};

export default withLoader(PostsList, axios);

