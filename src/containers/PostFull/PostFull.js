import React, {Component} from 'react';
import {Button, Grid, Jumbotron} from "react-bootstrap";
import axios from 'axios';

class PostFull extends Component {

    state = {
        post: {
            body: '',
            title: ''
        },
        loading: false
    };

    id = this.props.match.params.id;

    getPost () {
        axios.get(`/posts/${this.id}.json`).then(response => {
            console.log(response.data);
            this.setState({post: response.data});

        })
    }

    componentDidMount () {
        this.getPost();
    }

    deletePost = () => {
        axios.delete(`/posts/${this.id}.json`).then(() => {
            this.getPost();
            this.props.history.replace('/');
        })
    };

    editPost = () => {
        this.props.history.replace(`/posts/${this.id}/edit`);
    };


    render () {
        return (
            <Grid>
                <Jumbotron>
                    <h2>{this.state.post.title}</h2>
                    <h6 className="PostShort">Created on {this.state.post.date}</h6>
                    <p>{this.state.post.body}</p>
                    <p>
                        <Button bsStyle="primary" onClick={this.deletePost}>Delete</Button>
                        <Button bsStyle="primary" onClick={this.editPost}>Edit</Button>
                    </p>
                </Jumbotron>
            </Grid>
        )
    }

};

export default PostFull;