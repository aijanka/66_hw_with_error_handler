import React, {Component} from 'react';
import {Grid} from "react-bootstrap";
import axios from 'axios';
import InputPostForm from "../../components/InputPostForm/InputPostForm";
import moment from "moment";
import Spinner from "../../components/UI/Spinner/Spinner";

class PostAdd extends Component {
    state = {
        post: {
            title: '',
            body: '',
            date:  moment().format("dddd, MMMM Do YYYY, h:mm:ss a")
        },
        loading: false

    };

    saveCurrentValue = event => {
        const post = {...this.state.post};
        post[event.target.name] = event.target.value;
        this.setState({post});
    };

    saveFullPost = event => {
        event.preventDefault();
        this.setState({loading: true});
        axios.post('/posts.json', this.state.post).then(() => {
            this.setState({loading: false});
            this.props.history.replace('/');
        })

    };


    render() {
        return (
            <Grid>
                {this.state.loading ? <Spinner/> : <InputPostForm
                                                        typed={this.saveCurrentValue}
                                                        submit={this.saveFullPost}
                                                    />}
            </Grid>
        )
    }
}

export default PostAdd;