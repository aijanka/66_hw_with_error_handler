import React, {Component} from 'react';
import axios from 'axios';
import {Grid} from "react-bootstrap";
import InputPostForm from "../../components/InputPostForm/InputPostForm";
import Spinner from "../../components/UI/Spinner/Spinner";

class PostEdit extends Component {
    state = {
        post: {
            title: '',
            body: ''
        }
    };

    id = this.props.match.params.id;

    componentDidMount () {
        axios.get(`/posts/${this.id}.json`).then(response => {
            this.setState({post: response.data});
        })

    }

    saveCurrentValue = event => {
        const post = {...this.state.post};
        post[event.target.name] = event.target.value;
        this.setState({post});
    };

    editPost = (event) => {
        event.preventDefault();
        this.setState({loading: true});
        axios.patch(`/posts/${this.id}.json`, this.state.post).then(() => {
            this.setState({loading: false});
            this.props.history.replace('/posts/' + this.id);
        })
    }

    render() {
        return (
            <Grid>
                {this.state.loading ? <Spinner/> : <InputPostForm
                                                        typed={this.saveCurrentValue}
                                                        titleValue={this.state.post.title}
                                                        bodyValue={this.state.post.body}
                                                        submit={this.editPost}
                                                    />}

            </Grid>
        )
    }
}

export default PostEdit;