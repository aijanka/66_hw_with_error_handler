import React, {Component} from 'react';
import {Grid, Jumbotron} from "react-bootstrap";
import axios from 'axios';

class About extends Component {
    state = {
        about: ''
    };

    componentDidMount () {
        axios.get('/about.json').then(response => {
            this.setState({about: response.data});
        })
    }

    render() {
        return (
            <Grid>
                <Jumbotron>
                    <p>{this.state.about}</p>
                </Jumbotron>
            </Grid>

        );
    }

}

export default About;