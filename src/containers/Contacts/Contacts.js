import React, {Component} from 'react';
import {Grid, Panel} from "react-bootstrap";
import axios from 'axios';

class Contacts extends Component {
    state = {
        contacts: ''
    };

    componentDidMount () {
        axios.get('/contacts.json').then(response => {
            console.log(response.data);
            this.setState({contacts: response.data});
        })
    }

    render() {
        return (
            <Grid>
                <Panel>
                    <Panel.Body>{this.state.contacts}</Panel.Body>
                </Panel>
            </Grid>

        );
    }

}

export default Contacts;