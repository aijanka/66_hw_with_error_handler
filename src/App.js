import React, {Component, Fragment} from 'react';
import {NavLink, Route, Switch} from "react-router-dom";
import {Nav, Navbar, NavItem} from "react-bootstrap";
import PostsList from "./containers/PostsList/PostsList";
import PostFull from "./containers/PostFull/PostFull";
import PostAdd from "./containers/PostAdd/PostAdd";
import PostEdit from "./containers/PostEdit/PostEdit";
import Contacts from "./containers/Contacts/Contacts";
import About from "./containers/About/About";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Navbar inverse collapseOnSelect>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <NavLink to='/'>My blog</NavLink>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>

                        <Nav pullRight>
                            <NavItem href='/'>Home</NavItem>
                            <NavItem href='/postAdd'>Add a post</NavItem>
                            <NavItem href='/contacts'>Contacts</NavItem>
                            <NavItem href='/about'>About</NavItem>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

                <Switch>
                    <Route path='/' exact component={PostsList}/>
                    <Route path='/posts' exact component={PostsList}/>
                    <Route path='/postAdd' exact component={PostAdd}/>
                    <Route path='/contacts' exact component={Contacts}/>
                    <Route path='/about' exact component={About}/>
                    <Route path='/posts/:id' exact component={PostFull}/>
                    <Route path='/posts/:id/edit' exact component={PostEdit}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;
