import React from 'react';
import {Button, ControlLabel, FormControl, FormGroup} from "react-bootstrap";

const InputPostForm = props => (
    <form>
        <ControlLabel>Title</ControlLabel>
        <FormControl
            type="text"
            name='title'
            placeholder="Enter title of the post"
            onChange={props.typed}
            value={props.titleValue}
        />

        <FormGroup controlId="formControlsTextarea">
            <ControlLabel>Full Text</ControlLabel>
            <FormControl
                componentClass="textarea"
                placeholder="Enter full post description"
                name='body'
                onChange={props.typed}
                value={props.bodyValue}
            />
        </FormGroup>


        <Button type="submit" onClick={props.submit}>Submit</Button>
    </form>
);

export default InputPostForm;
