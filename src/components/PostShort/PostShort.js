import React from 'react';
import {Button, Jumbotron} from "react-bootstrap";

const PostShort = props => {
    const link = `/posts/${props.id}`;
    return (
        <Jumbotron>
            <div className="postShort">
                <h6 className="PostShort">Created on {props.date}</h6>
                <h3>{props.title}</h3>
                <Button href={link}>Read more</Button>
            </div>
        </Jumbotron>


    )
};

export default PostShort;